const dotenv = require('dotenv').config();
const express = require("express");
const app = express();
const router = require('./router');
const cors = require('cors');
const morgan = require('morgan');

app.use(cors());
app.use(express.json());

app.use('/api/v1', router);

module.exports = app;