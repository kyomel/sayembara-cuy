const users = require('../models');
const jwt = require('jsonwebtoken');

module.exports = async function (req,res,next){
    try{
        let token = req.headers.authorization;
        payload = await jwt.verify(token, process.env.SECRET_KEY);

        let user = await users.findByPk(payload.id)
        req.user = user;
        next();
    }
    catch (err) {
        res.status(401).json({
            status: 'Failed',
            message: 'Unauthorized login'
        })
    }
}