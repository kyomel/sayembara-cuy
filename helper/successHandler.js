module.exports = (res, code, data) => {
    res.status(code).json({
      status: 'success',
      data: data
    })
  }
