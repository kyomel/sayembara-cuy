const router = require('express').Router();
const userController = require('./controller/userController');
const authentication =require('./middleware/authenticate');


//User router
router.post('/user/register', userController.register);
router.post('/user/login', userController.login);
router.get('/user/profile', authentication, userController.profile);


module.exports = router;