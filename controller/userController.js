const { users, profiles } = require('../models');
const bcrypt = require('bcryptjs');
const errorResponse = require('../helper/errorHandler');
const successResponse = require('../helper/successHandler');
const token = require('../helper/token');
const validator = require('fastest-validator');
const val = new validator();

exports.register = async function register(req,res){
    let { 
        first_name, 
        last_name,
        email,
        password,
        verify_password,
        role
    } = req.body
    try {
        const allowedInput = {
            first_name: { type: 'string', required: true},
            last_name: { type: 'string', required: true},
            email: { type: 'string', empty: false},
            password: { type: 'string', required: true, length: 8 },
            role: { type: "enum", values: ["participant", "provider", "admin"], empty: false}
        }
        const isValid = val.validate(req.body, allowedInput)
        if (Array.isArray(isValid)) return res.status(400).json({
            status:'fail',
            errors: isValid
        })
        const nameExist = await profiles.findOne ({
            where: {
                first_name,
                last_name
            }
        })
        if(nameExist){
            throw new Error('The username is not available');
        }

        const emailExist = await users.findOne({
            where: {
                email
            }
        })
        if(emailExist){
            throw new Error('The email is already taken');
        }
        if(password !== verify_password) throw new Error('Sorry password does not match, please try again!');
        
        await users.create({
            email,
            password,
            role
        })
        await profiles.create({
            first_name,
            last_name,
            user_id: users.id
        })

        return successResponse(res,201,data)
    }
    catch (err) {
        error(res,422,err.message)
    }
}

exports.login = async function login(req, res) {
    try {
        let instance = await users.findOne({
            where: {
                email: req.body.email.toLowerCase()
            }
        })
        if(!instance) {
            throw new Error(`email ${req.body.email} doesn't exist!`)
        }
        const isPasswordTrue = await bcrypt.compareSync(req.body.password, instance.password)
        if(!isPasswordTrue) {
            throw new Error(`Wrong Password!`)
        }
        return successResponse(res, 200, { token: token(instance)})
    } catch (err) {
        errorResponse(res,403, [err.message])
    }
}

exports.profile = async function profile(req,res){
    const data = await users.findByPk(req.users.id, {
        attributes: ['id', 'email', 'role'],
        include: [{
            model: profiles,
            attributes: ['id', 'first_name', 'last_name']
        }]
    })
    successResponse(res, 200, data)
}