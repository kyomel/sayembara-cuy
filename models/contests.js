'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class contests extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      contests.belongsTo(models.payments, {
        foreignKey: 'payment_id'
      }),
      contests.hasMany(models.submissions, {
        foreignKey: 'contest_id'
      }),
      contests.belongsTo(models.users, {

        foreignKey: 'user_id'
      })
    }
  };
  contests.init({
    title: DataTypes.STRING,
    prize: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: {
          arg: true,
          msg: 'Prize only allowed number!'
        }
      }
    },
    due_date: DataTypes.DATE,
    announcement: DataTypes.DATE,
    description: {
      type: DataTypes.TEXT,
      validate: {
        is: {
          arg: /^[a-z]+$/i,
          msg: 'Description only allow letters'
        }
      }
    },
    status: {
      type: DataTypes.STRING,
      defaultValue: 'pending'
    },
    winner: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    user_id: DataTypes.INTEGER,
    payment_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'contests',
  });
  return contests;
};