'use strict';
const bcrypt = require('bcryptjs');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      users.hasOne(models.profiles, {
        foreignKey: 'user_id'
      }),
      users.hasOne(models.submissions, {
        foreignKey: 'user_id'
      }),
      users.hasMany(models.contest, {
        foreignKey: 'user_id'
      })
    }
  };
  users.init({
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {
          msg: "It must be an email!"
        }
      },
      unique: {
        args: true,
        msg:"Email already registered!"
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [8],
          msg: "Password should be at least 8 characters!"
        }
      }
    },
    role: DataTypes.STRING
  }, {
    hooks: {
      beforeValidate: instance => {
        instance.email = instance.email.toLowerCase();
      },
      beforeCreate: instance => {
        instance.password = bcrypt.hashSync(instance.password, 10);
      }
    },
    sequelize,
    modelName: 'users',
  });
  return users;
};