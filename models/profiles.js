'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class profiles extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      profiles.belongsTo(models.users, {
        foreignKey: 'user_id'
      })
    }
  };
  profiles.init({
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    bank: {
      type: DataTypes.STRING,
      defaultValue: 'Bank XYZ'
    },
    acc_number: {
      type: DataTypes.INTEGER,
      defaultValue: 123456
    },
    picture: {
      type: DataTypes.TEXT,
      defaultValue: 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png'
    },
    user_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'profiles',
  });
  return profiles;
};