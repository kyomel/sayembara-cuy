'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class submissions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      submissions.belongsTo(models, users, {
        foreignKey: 'user_id'
      }),
      submissions.belongsTo(models.contest, {
        foreignKey: 'contest_id'
      })
    }
  };
  submissions.init({
    file: {
      type: DataTypes.ARRAY(DataTypes.TEXT),
      validate: {
        min: 1
      }
    },
    description: DataTypes.TEXT,
    user_id: DataTypes.INTEGER,
    contest_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'submissions',
  });
  return submissions;
};