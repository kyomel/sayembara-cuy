'use strict';
const bcrypt = require('bcryptjs');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.bulkInsert('profiles', [{
        first_name: 'admin',
        last_name: 'ganteng',
        bank: 'Bank ABC',
        acc_number: '123456789',
        picture: 'https://www.awesomegreece.com/wp-content/uploads/2018/10/default-user-image.png',
        user_id: '1',
        createdAt: new Date(),
        updatedAt: new Date()
      }]),
      queryInterface.bulkInsert('users', [{
        role: 'admin',
        email: 'admin1@mail.com',
        password: bcrypt.hashSync('12345678', bcrypt.genSaltSync(10)),
        createdAt: new Date(),
        updatedAt: new Date(),
      }])
      
    ], {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.bulkDelete('profiles', null, {}),
      queryInterface.bulkDelete('users', null, {})
    ])
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
  }
};
